(function() {
    new Slider("#slider .slide").options({
        prev: "#slider nav button.arrow-prev",
        next: "#slider nav button.arrow-next",
    }).init();
})();